# ex1.py
# -----------------------

"""
Python Code Snippets
Daria Tolmacheva

Purpose: Computer Vision Lab 2.
"""
# import the necessary packages
import numpy as np
from matplotlib import pyplot as plt
import argparse
import cv2
from PIL.Image import *
from scipy import ndimage
from scipy import spatial
import math

#def harrisPointsDetector(image)


def Harris(im):
    neim = im
    imarr = np.asarray(neim, dtype=np.float64)
    imarr = cv2.copyMakeBorder(imarr, 3, 3, 3, 3, cv2.BORDER_REFLECT)
    ix = ndimage.sobel(imarr, 0)
    iy = ndimage.sobel(imarr, 1)
    ix2 = ix * ix
    iy2 = iy * iy
    ixy = ix * iy
    #ix2 = ndimage.gaussian_filter(ix2, sigma=0.5)
    #iy2 = ndimage.gaussian_filter(iy2, sigma=0.5)
    #ixy = ndimage.gaussian_filter(ixy, sigma=0.5)
    ix2 = cv2.GaussianBlur(ix2, (5,5), 0.5, 0.5)
    iy2 = cv2.GaussianBlur(iy2, (5,5), 0.5, 0.5)
    ixy = cv2.GaussianBlur(ixy, (5,5), 0.5, 0.5)
    c, l = imarr.shape
    result = np.zeros((c, l))
    r = np.zeros((c, l))
    rmax = 0
    kparr = []

    for i in range(3, (c-3)):
        for j in range(3, (l-3)):
            m = np.array([[ix2[i, j], ixy[i, j]], [ixy[i, j], iy2[i, j]]], dtype=np.float64)
            r[i, j] = np.linalg.det(m) - 0.1 * (np.power(np.trace(m), 2))
            if r[i, j] > rmax:
                rmax = r[i, j]

    # counts = []
    # thresholds = []
    # for thr in range (1, 101):
    #     counts.append((r > thr * 0.001 * rmax).sum())
    #     thresholds.append(thr * 0.001)
    # plt.plot(thresholds, counts)
    # plt.xlabel('x - Threshold as a fraction of max c(H)')
    # plt.ylabel('y - Number of keypoints')
    # plt.title('Number of keypoints based on the threshold')
    # plt.show()

    for i in range(3, (c-3)):
        for j in range(3, (l-3)):
            nghb = np.array(r[i-3:i+4, j-3:j+4])
            if r[i, j] == np.amax(nghb) and r[i, j] > 0.01 * rmax:
                # result[i, j] =
                #result for visualisation
                result[i, j] = math.atan(iy[i, j]/ix[i, j])
                #result for later use
                kparr.append(cv2.KeyPoint(j, i, 7.0, math.atan(iy[i, j]/ix[i, j])))


    # pc, pr = np.where(result == 1)
    # pc, pr = np.where(result != 0)
    # plt.plot(pr, pc, 'r+')
    # plt.savefig('harris_test.png')
    # plt.imshow(im, 'gray')
    # plt.show()
    # plt.imsave('harris_test.png', im, 'gray')
    kparr = np.array(kparr)
    return kparr

def DescriptorsMine(img, kparr):
    # Initiate ORB detector
    orb = cv2.ORB_create()
    # compute the descriptors with ORB
    kp, des = orb.compute(img, kparr)
    # draw only keypoints location,not size and orientation
    # img2 = cv2.drawKeypoints(img, kp, None, color=(255,0,0), flags=0)
    # plt.imshow(img2), plt.show()
    return kp, des

def DescriptorsHarris(img):
    # Initiate ORB detector
    orb = cv2.ORB_create()
    # find the keypoints with ORB
    kp = orb.detect(img,None)
    # compute the descriptors with ORB
    kp, des = orb.compute(img, kp)
    # draw only keypoints location,not size and orientation
    img2 = cv2.drawKeypoints(img, kp, None, color=(255,0,0), flags=0)
    plt.imshow(img2), plt.show()
    return kp, des

def DescriptorsFast(img):
    # Initiate ORB detector
    orb = cv2.ORB_create(scoreType=cv2.ORB_FAST_SCORE)
    # find the keypoints with ORB
    kp = orb.detect(img,None)
    # compute the descriptors with ORB
    kp, des = orb.compute(img, kp)
    # draw only keypoints location,not size and orientation
    img2 = cv2.drawKeypoints(img, kp, None, color=(255,0,0), flags=0)
    plt.imshow(img2), plt.show()
    return kp, des


def matchFeaturesssd(desc1, desc2):
        matches = []
        n1 = desc1.shape[0]
        n2 = desc2.shape[0]
        distance = spatial.distance.cdist(desc1, desc2, 'euclidean')
        match = np.argmin(distance, 1)
        # print(match)
        for i in range(n1):
            f = cv2.DMatch()
            f.queryIdx = i
            f.trainIdx = int(match[i])
            f.distance = distance[i, int(match[i])]
            matches.append(f)
        return matches

def matchFeatures(desc1, desc2):
        matches = []
        distance = spatial.distance.cdist(desc1, desc2, 'euclidean')
        for col,row in enumerate(distance):
            trainIdx = col
            rowIdx = np.argsort(row)
            first= rowIdx[0]
            second = rowIdx[1]
            minest = row[first]
            minsec = row[second]
            ratio = minest / float(minsec)
            matches.append(cv2.DMatch(trainIdx, first, ratio))
        return matches

# Open image
img1 = cv2.imread('bernieSanders.jpeg', cv2.IMREAD_GRAYSCALE)
# img2 = cv2.imread('bernie180.jpeg', cv2.IMREAD_GRAYSCALE)
img3 = cv2.imread('bernieBenefitBeautySalon.jpeg', cv2.IMREAD_GRAYSCALE)
# img4 = cv2.imread('BernieFriends.png', cv2.IMREAD_GRAYSCALE)
# img5 = cv2.imread('bernieMoreblurred.jpeg', cv2.IMREAD_GRAYSCALE)
# img6 = cv2.imread('bernieNoisy2.png', cv2.IMREAD_GRAYSCALE)
# img7 = cv2.imread('berniePixelated2.png', cv2.IMREAD_GRAYSCALE)
# img8 = cv2.imread('bernieShoolLunch.jpeg', cv2.IMREAD_GRAYSCALE)
# img9 = cv2.imread('brighterBernie.jpeg', cv2.IMREAD_GRAYSCALE)
# img10 = cv2.imread('darkerBernie.jpeg', cv2.IMREAD_GRAYSCALE)

print(img1.ndim)
print(img1.shape)

# Display image
# cv2.namedWindow('Source Image')
# cv2.imshow('Source Image', img)

kparr = Harris(img1)
kp1, des1 = DescriptorsMine(img1, kparr)
kparr3 = Harris(img3)
kp3, des3 = DescriptorsMine(img3, kparr3)
matches3 = matchFeatures(des1, des3)
final_img3 = cv2.drawMatches(img1, kp1, img3, kp3, matches3, None)
# Show the final image
cv2.namedWindow('Matches3 mine')
cv2.imshow("Matches3 mine", final_img3)

matches3d = matchFeaturesssd(des1, des3)
final_img3d = cv2.drawMatches(img1, kp1, img3, kp3, matches3d, None)
# Show the final image
cv2.namedWindow('Matches3 mine matchFeaturesssd')
cv2.imshow("Matches3 mine matchFeaturesssd", final_img3d)


kp1, des1 = DescriptorsFast(img1)
kp3, des3 = DescriptorsFast(img3)

matches3d = matchFeatures(des1, des3)
final_img3d = cv2.drawMatches(img1, kp1, img3, kp3, matches3d, None)
# Show the final image
cv2.namedWindow('Matches3 fast')
cv2.imshow("Matches3 default", final_img3d)

matches3d = matchFeaturesssd(des1, des3)
final_img3d = cv2.drawMatches(img1, kp1, img3, kp3, matches3d, None)
# Show the final image
cv2.namedWindow('Matches3 fast matchFeaturesssd')
cv2.imshow("Matches3 fast matchFeaturesssd", final_img3d)

# kparr = Harris(img1)
# DescriptorsMine(img1, kparr)
# print('done DescriptorsMine')
# DescriptorsHarris(img1)
# print('done DescriptorsHarris')
# DescriptorsFast(img1)
# print('done DescriptorsFast')

# kp1, des1 = DescriptorsFast(img1)
# kp2, des2 = DescriptorsFast(img2)
# kp3, des3 = DescriptorsFast(img3)
# kp4, des4 = DescriptorsFast(img4)
# kp5, des5 = DescriptorsFast(img5)
# kp6, des6 = DescriptorsFast(img6)
# kp7, des7 = DescriptorsFast(img7)
# kp8, des8 = DescriptorsFast(img8)
# kp9, des9 = DescriptorsFast(img9)
# kp10, des10 = DescriptorsFast(img10)
#
# matches2 = matchFeaturesssd(des1, des2)
# matches3 = matchFeaturesssd(des1, des3)
# matches4 = matchFeaturesssd(des1, des4)
# matches5 = matchFeaturesssd(des1, des5)
# matches6 = matchFeaturesssd(des1, des6)
# matches7 = matchFeaturesssd(des1, des7)
# matches8 = matchFeaturesssd(des1, des8)
# matches9 = matchFeaturesssd(des1, des9)
# matches10 = matchFeaturesssd(des1, des10)

# final_img2 = cv2.drawMatches(img1, kp1, img2, kp2, matches2, None)
# # Show the final image
# cv2.namedWindow('Matches2')
# cv2.imshow("Matches2", final_img2)
#
# final_img3 = cv2.drawMatches(img1, kp1, img3, kp3, matches3, None)
# # Show the final image
# cv2.namedWindow('Matches3')
# cv2.imshow("Matches3", final_img3)
#
# final_img4 = cv2.drawMatches(img1, kp1, img4, kp4, matches4, None)
# # Show the final image
# cv2.namedWindow('Matches4')
# cv2.imshow("Matches4", final_img4)

# Wait for spacebar press before closing,
# otherwise window will close without you seeing it
while True:
    if cv2.waitKey(1) == ord(' '):
        break

cv2.destroyAllWindows()
