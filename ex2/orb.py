import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('bernieSanders.jpeg', cv2.IMREAD_GRAYSCALE)

print('read image')

# Initiate STAR detector
orb = cv2.ORB()

print('Initiated STAR detector')

# find the keypoints with ORB
kp = orb.detect(img, None)

print('found the keypoints')

# compute the descriptors with ORB
kp, des = orb.compute(img, kp)

print('computed the descriptors')

# draw only keypoints location,not size and orientation
img2 = cv2.drawKeypoints(img,kp,color=(0,255,0), flags=0)
plt.imshow(img2),plt.show()
