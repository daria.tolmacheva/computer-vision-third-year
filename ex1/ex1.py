# ex1.py
# -----------------------

"""
Python Code Snippets
Daria Tolmacheva

Purpose: Computer Vision Lab 1.
"""
# import the necessary packages
import numpy as np
from matplotlib import pyplot as plt
import argparse
import cv2

def convolve(image, kernel):

    #change image to 2D since the third dimension
    # consists of 3 identical numbers
    #image2d = image[:,:,0]
    image2d = image

    #flip the kernel
    kernel = np.flipud(np.fliplr(kernel))
    #initialise function output
    output = np.zeros_like(image)

    #add zero padding
    image_padded = np.zeros((image2d.shape[0] + 2, image2d.shape[1] + 2))
    image_padded[1:-1, 1:-1] = image2d

    #loop over every pixel of the image
    for x in range(image2d.shape[1]):
        for y in range(image2d.shape[0]):
            #apply kernel to each pixel
            output[y, x]=(kernel * image_padded[y: y+3, x: x+3]).sum()

    return output



# Open bmp image
img = cv2.imread('kitty.bmp', cv2.IMREAD_GRAYSCALE)

print(img.ndim)
print(img.shape)

# Display image
cv2.namedWindow('Source Image')
cv2.imshow('Source Image', img)

smallBlur = np.array((
	[1, 1, 1],
	[1, 1, 1],
	[1, 1, 1]), dtype="float")
smallBlur = smallBlur * 1/9

weightedBlur = np.array((
	[1, 2, 1],
	[2, 4, 2],
	[1, 2, 1]), dtype="float")
weightedBlur = weightedBlur * 1/16

weightedBlur2 = np.array((
	[0.08, 0.12, 0.08],
	[0.12, 0.2, 0.12],
	[0.08, 0.12, 0.08]), dtype="float")

img2 = convolve(img, smallBlur)
cv2.imshow('averageBlur', img2)

img3 = convolve(img, weightedBlur)
cv2.imshow('weightedBlur', img3)

img4 = convolve(img, weightedBlur2)
cv2.imshow('weightedBlur2', img4)

vertical = np.array((
	[-1, 0, 1],
	[-1, 0, 1],
	[-1, 0, 1]), dtype="float")
vertical = vertical * 1/3

horisontal = np.array((
	[-1, -1, -1],
	[0, 0, 0],
	[1, 1, 1]), dtype="float")
horisontal = horisontal * 1/3
'''
img5 = convolve(img, horisontal)
cv2.imshow('horisontal', img5)

img6 = convolve(img, vertical)
cv2.imshow('vertical', img6)


imgcomb11 = cv2.add(img5, img6)
cv2.imshow('comb horizontal + vertical', imgcomb11)

imgblurhor = convolve(img4, horisontal)
cv2.imshow('bl hor', imgblurhor)
imgblurver = convolve(img4, vertical)
cv2.imshow('bl ver', imgblurver)
imgcomb12 = cv2.add(imgblurhor, imgblurver)
cv2.imshow('comb bl horizontal + vertical', imgcomb12)
'''
'''
horisontalSobel = np.array((
	[-1, -2, -1],
	[0, 0, 0],
	[1, 2, 1]), dtype="float")
horisontalSobel = horisontalSobel * 1/4

verticalSobel = np.array((
	[-1, 0, 1],
	[-2, 0, 2],
	[-1, 0, 1]), dtype="float")
verticalSobel = verticalSobel * 1/4

img7 = convolve(img, horisontalSobel)
cv2.imshow('horisontalSobel', img7)

img8 = convolve(img, verticalSobel)
cv2.imshow('verticalSobel', img8)

abs_grad_x1 = cv2.convertScaleAbs(img7)
abs_grad_y1 = cv2.convertScaleAbs(img8)
imgcomb21 = cv2.addWeighted(abs_grad_x1, 0.5, abs_grad_y1, 0.5, 0)
#imgcomb21 = cv2.add(img7, img8)
cv2.imshow('comb horisontalSobel + verticalSobel', imgcomb21)

imgblurhors = convolve(img4, horisontalSobel)
cv2.imshow('blur horisontalSobel', imgblurhors)
imgblurvers = convolve(img4, verticalSobel)
cv2.imshow('blur verticalSobel', imgblurvers)

abs_grad_x12 = cv2.convertScaleAbs(imgblurhors)
abs_grad_y12 = cv2.convertScaleAbs(imgblurvers)
imgcomb22 = cv2.addWeighted(abs_grad_x12, 0.5, abs_grad_y12, 0.5, 0)
#imgcomb22 = cv2.add(imgblurhors, imgblurvers)
cv2.imshow('comb s horizontal + vertical', imgcomb22)'''


# Sobel edge detection (combine horiz. and vert. edges)
grad_x = cv2.Sobel(img, cv2.CV_16S, 1, 0)
grad_y = cv2.Sobel(img, cv2.CV_16S, 0, 1)
#
abs_grad_x = cv2.convertScaleAbs(grad_x)
abs_grad_y = cv2.convertScaleAbs(grad_y)
grad = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
#
output = grad.astype(np.uint8)
cv2.imshow('x', abs_grad_x)
cv2.imshow('y', abs_grad_y)
cv2.imshow('x+y', output)

# Sobel edge detection (combine horiz. and vert. edges)
#blur = cv2.GaussianBlur(img, (3,3), 1)
grad_x1 = cv2.Sobel(img4, cv2.CV_16S, 1, 0)
grad_y1 = cv2.Sobel(img4, cv2.CV_16S, 0, 1)
#
abs_grad_x1 = cv2.convertScaleAbs(grad_x1)
abs_grad_y1 = cv2.convertScaleAbs(grad_y1)
grad1 = cv2.addWeighted(abs_grad_x1, 0.5, abs_grad_y1, 0.5, 0)
#
output1 = grad1.astype(np.uint8)
cv2.imshow('blur', img4)
cv2.imshow('blur x', abs_grad_x1)
cv2.imshow('blur y', abs_grad_y1)
cv2.imshow('blur x+y', output1)

_, output2 = cv2.threshold(output1, 80, 255, cv2.THRESH_BINARY)
#T, output2 = cv2.threshold(output, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.imshow('thres', output2)
#print(T)
#_, output2 = cv2.threshold(output1, 150, 255, cv2.THRESH_BINARY)
#cv2.imshow('blur thres', output1)

# Calculate the histogram
hist = cv2.calcHist([output], [0], None, [256], [0, 256])
hist = hist.reshape(256)

# Plot histogram
plt.bar(np.linspace(0,255,256), hist)
plt.title('Histogram')
plt.ylabel('Frequency')
plt.xlabel('Grey Level')
plt.show()

# Calculate the histogram
hist = cv2.calcHist([output1], [0], None, [256], [0, 256])
hist = hist.reshape(256)

# Plot histogram
plt.bar(np.linspace(0,255,256), hist)
plt.title('HistogramBlur')
plt.ylabel('Frequency')
plt.xlabel('Grey Level')
plt.show()

'''
laplacial = np.array((
	[0, 1, 0],
	[1, -4, 1],
	[0, 1, 0]), dtype="float")

img9 = convolve(img, laplacial)
cv2.imshow('laplacial', img9)

img10 = convolve(img, weightedBlur2)
img10 = convolve(img10, laplacial)
cv2.imshow('blur + laplacial', img10)


# Calculate the histogram
hist = cv2.calcHist([img], [0], None, [256], [0, 256])
hist = hist.reshape(256)

# Plot histogram
plt.bar(np.linspace(0,255,256), hist)
plt.title('Histogram')
plt.ylabel('Frequency')
plt.xlabel('Grey Level')
plt.show()'''

# Wait for spacebar press before closing,
# otherwise window will close without you seeing it
while True:
    if cv2.waitKey(1) == ord(' '):
        break

cv2.destroyAllWindows()
